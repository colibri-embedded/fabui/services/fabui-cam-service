#include "config.h"
#include <CLI/CLI.hpp>
#include <iostream>
#include <spdlog/sinks/rotating_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include "cam_service_wamp_wrapper.hpp"

void show_version(size_t)
{
    std::cout << PACKAGE_STRING << std::endl;
    throw CLI::Success();
}

std::shared_ptr<spdlog::logger> init_logging(const std::string& filename,
                                             spdlog::level::level_enum log_level = spdlog::level::warn)
{
    // Create a file rotating logger with 5mb size max and 3 rotated files
    auto file_sink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(filename, 1024 * 1024 * 5, 3);
    file_sink->set_level(log_level);

    auto console_sink = std::make_shared<spdlog::sinks::stderr_color_sink_mt>();
    console_sink->set_level(log_level);

    std::vector<spdlog::sink_ptr> sinks{ file_sink, console_sink };

    auto logger = std::make_shared<spdlog::logger>("logger", sinks.begin(), sinks.end());
    logger->set_level(log_level);
    logger->set_pattern("[%Y-%m-%d %H:%M:%S.%e] [%l] %v");

    spdlog::register_logger(logger);

    return logger;
}

int main(int argc, char** argv)
{
    CLI::App appargs{ PACKAGE_NAME };

    std::string uri       = "ws://127.0.0.1:9001";
    std::string realm     = "aries";
    std::string env_file  = "/var/www/.env";
    std::string log_file  = "/var/log/fabui/" PACKAGE_NAME ".log";
    std::string log_level = "warning";

    appargs.add_option("-u,--uri", uri, "WAMP default realm [default: " + uri + "]");
    appargs.add_option("-r,--realm", realm, "WAMP default realm [default: " + realm + "]");
    appargs.add_option("-e,--backend-env", env_file, "ARIES web .env file [default: " + env_file + "]");
    appargs.add_option("-l,--log-file", log_file, "Log to file [default: " + log_file + "]");
    appargs.add_option("-L,--log-level",
                       log_level,
                       "Log level (trace, debug, info, warning, error, critical, off) [default: " + log_level + "]");
    appargs.add_config("-c,--config", "config.ini", "Read config from an ini file", false);
    appargs.add_flag_function("-v,--version", show_version, "Show version");

    try {
        appargs.parse(argc, argv);
    } catch (const CLI::ParseError& e) {
        return appargs.exit(e);
    }

    auto logger = init_logging(log_file, spdlog::level::from_str(log_level));

    try {
        pthread_setname_np(pthread_self(), "main");

        fabui::CamService service;
        fabui::CamServiceWampWrapper wampservice(env_file, service);

        unsigned retry = 5;

        while (retry) {
            if (wampservice.connect(uri, realm)) {
                auto finished = wampservice.finished_future();
                finished.wait();
            } else {
                --retry;
                logger->error("Failed to connect to router, {} attempts left", retry);
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
        }
    } catch (const std::exception& e) {
        logger->error("Service startup: {}", e.what());
        return 1;
    }

    return 0;
}