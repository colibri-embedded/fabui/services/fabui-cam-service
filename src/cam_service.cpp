/**
 * fabui-cam-service
 * Copyright (C) 2020  Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file cam_service.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "cam_service.hpp"

// section:manual_includes
// endsection:manual_includes

namespace fabui {

CamService::CamService()

{
    // section:<create>_2a3wkxvopc77mcy5mi5yaxbnpq

    /* add your implementation here */

    // endsection:<create>_2a3wkxvopc77mcy5mi5yaxbnpq
}

void CamService::start()
{
    // section:start_5ivsm5wcrqg3e3jzgmndg3dlsi
    using namespace std::placeholders;
    m_running          = true;
    m_publisher_thread = std::thread(&CamService::publisherThreadLoop, this);
    // endsection:start_5ivsm5wcrqg3e3jzgmndg3dlsi
}

void CamService::stop()
{
    // section:stop_544zwlkenozxw7bsvuwmdnqelm
    {
        std::unique_lock<std::mutex> lock(m_running_mut);
        m_running = false;
    }

    publish("", {});

    if (m_publisher_thread.joinable())
        m_publisher_thread.join();
    // endsection:stop_544zwlkenozxw7bsvuwmdnqelm
}

wampcc::json_object CamService::getInfo()
{
    // section:get_info_esxrnntfxuxvhvepfjzydo6uxy
    return {};
    // endsection:get_info_esxrnntfxuxvhvepfjzydo6uxy
}

void CamService::publisherThreadLoop()
{
    // section:publisher_thread_loop_bxb36w7vfhgs6f5b3wh5gcd4r4
    pthread_setname_np(pthread_self(), "publisher");
    m_logger->debug("PublisherThread: started");

    while (true) {
        {
            std::lock_guard<std::mutex> guard(m_running_mut);
            if (!m_running)
                return;
        }

        auto pub = m_pq.pop();
        if (!pub.topic.empty()) {
            m_session->publish(pub.topic, pub.args, pub.options);
        }
    }
    // endsection:publisher_thread_loop_bxb36w7vfhgs6f5b3wh5gcd4r4
}

void CamService::publish(const std::string& topic, const wampcc::wamp_args& args)
{
    // section:publish_mhcx5yfl2bg5prwtwyi73qs4a4
    m_pq.push({ topic, args, {} });
    // endsection:publish_mhcx5yfl2bg5prwtwyi73qs4a4
}

void CamService::setSession(IWampSession* value)
{
    m_session = value;
}

IWampSession* CamService::getSession() const
{
    return m_session;
}

} // namespace fabui

// section:manual_code
// endsection:manual_code