/**
 * fabui-cam-service
 * Copyright (C) 2020  Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file cam_service_wamp_wrapper.cpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#include "cam_service_wamp_wrapper.hpp"

// section:manual_includes
#include "config.h"
#define MAX_WORKERS 2
// endsection:manual_includes

namespace fabui {

CamServiceWampWrapper::CamServiceWampWrapper(const std::string& env_file, CamService& service)
  : BackendService(PACKAGE_STRING, env_file, MAX_WORKERS, false)
  , m_service(service)
{
    // section:<create>_6hgitx7orvgmofwz6ouxqd6i6e
    m_service.setSession(this);
    // endsection:<create>_6hgitx7orvgmofwz6ouxqd6i6e
}

void CamServiceWampWrapper::onConnect()
{

    join(realm(), { "token" }, "cam-service");
}

void CamServiceWampWrapper::onDisconnect() {}

void CamServiceWampWrapper::onJoin()
{
    using namespace std::placeholders;
    // wampcc provider implementation

    provide("cam.services.get_info", std::bind(&CamServiceWampWrapper::getInfoWrapped, this, _1));

    // section:onjoin_manual_code
    m_service.start();
    // endsection:onjoin_manual_code
}

void CamServiceWampWrapper::getInfoWrapped(const wampcc::invocation_info& info)
{

    try {
        // Get arguments
        // Run service method
        auto result = m_service.getInfo();
        yield(info.request_id, {}, result);
    } catch (const std::exception& e) {
        /* Return error */
        wampcc::json_array errors = { e.what() };
        error(info.request_id, "wamp.error.runtime_error", errors);
    }
}

} // namespace fabui

// section:manual_code
// endsection:manual_code