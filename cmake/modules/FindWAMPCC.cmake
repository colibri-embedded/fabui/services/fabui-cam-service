include(CheckLibraryExists)

find_package(PkgConfig)

pkg_check_modules(WAMPCC wampcc REQUIRED)
if(WAMPCC_FOUND)
  set(HAVE_WAMPCC ON)
  link_directories( ${WAMPCC_LIBRARY_DIRS} )
endif()