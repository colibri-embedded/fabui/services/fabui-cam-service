/**
 * fabui-cam-service
 * Copyright (C) 2020  Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file feeds_service.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef FEEDS_SERVICE_HPP
#define FEEDS_SERVICE_HPP

#include <fabui/wamp/session.hpp>
#include <string>
// section:manual_includes
// endsection:manual_includes

namespace fabui {

/**
 * Wamp Publication structure
 */
struct Publication
{
    /// Publication topic
    std::string topic;
    /// Publication data
    wampcc::wamp_args args;
    /// Publication options
    wampcc::json_object options;
};

} // namespace fabui

#endif /* FEEDS_SERVICE_HPP */