/**
 * fabui-cam-service
 * Copyright (C) 2020  Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file cam_service.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef CAM_SERVICE_HPP
#define CAM_SERVICE_HPP

#include "feeds_service.hpp"
#include <fabui/thread/queue.hpp>
#include <fabui/wamp/session.hpp>
#include <memory>
#include <mutex>
#include <spdlog/spdlog.h>
#include <string>
#include <thread>
// section:manual_includes
// endsection:manual_includes

namespace fabui {

/**
 * FABUI CAM service
 */
class CamService
{
    // section:manual_definitions
    // endsection:manual_definitions

  public:
    /**
     *
     */
    CamService();

    /**
     * Start service.
     */
    void start();

    /**
     * Stop service.
     */
    void stop();

    /**
     * Return network information
     * @return Object with info data
     */
    wampcc::json_object getInfo();

    /**
     * Set wamp session
     * @param[in] value
     */
    void setSession(IWampSession* value);

    /**
     * Get wamp session
     * @return
     */
    IWampSession* getSession() const;

  protected:
  private:
    /// WAMP session
    IWampSession* m_session;
    ///
    std::shared_ptr<spdlog::logger> m_logger;
    /// Monitor thread
    std::thread m_publisher_thread;
    /// Publish queue
    Queue<Publication> m_pq;
    /// Thread running mutex
    std::mutex m_running_mut;
    /// Thread running status
    bool m_running = false;
    /**
     * Publisher thread
     */
    void publisherThreadLoop();

    /**
     * Postponed publish
     * @param[in] topic Message topic
     * @param[in] args Message arguments
     */
    void publish(const std::string& topic, const wampcc::wamp_args& args);
};

} // namespace fabui

#endif /* CAM_SERVICE_HPP */