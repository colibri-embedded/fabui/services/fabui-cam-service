/**
 * fabui-cam-service
 * Copyright (C) 2020  Colibri-Embedded
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @file cam_service_wamp_wrapper.hpp
 *
 * @author Daniel Kesler <kesler.daniel@gmail.com>
 *
 */
#ifndef CAM_SERVICE_WAMP_WRAPPER_HPP
#define CAM_SERVICE_WAMP_WRAPPER_HPP

#include "cam_service.hpp"
#include <fabui/wamp/backend_service.hpp>
#include <fabui/wamp/session.hpp>
#include <string>
// section:manual_includes
// endsection:manual_includes

namespace fabui {

/**
 *
 */
class CamServiceWampWrapper : public BackendService
{
    // section:manual_definitions
    // endsection:manual_definitions

  public:
    /**
     *
     * @param[in] env_file
     * @param[in] service
     */
    CamServiceWampWrapper(const std::string& env_file, CamService& service);

    /**
     *
     */
    void onConnect();

    /**
     *
     */
    void onDisconnect();

    /**
     *
     */
    void onJoin();

    /**
     *
     * @param[in] info
     */
    void getInfoWrapped(const wampcc::invocation_info& info);

  protected:
  private:
    ///
    CamService& m_service;
};

} // namespace fabui

#endif /* CAM_SERVICE_WAMP_WRAPPER_HPP */